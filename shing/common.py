from pathlib import Path
import urllib.parse
import requests
import os
import typing as T

import ppt
from ppt.toolz import *

log = ppt.logging.new_log(__name__)

base_url = 'https://shing.tv/'

def shing_url(*parts):
    '''
    Examples:

    >>> shing_url('test')
    'https://shing.tv/test'
    '''
    return urllib.parse.urljoin(
        base_url, '/'.join(parts)
    )

login_url = shing_url('/auth/login')
Username = T.NewType('Username', str)
Password = T.NewType('Password', str)
Creds = T.Tuple[Username, Password]

def get_creds_from_env() -> Creds:
    return (
        os.environ['SHING_USERNAME'],
        os.environ['SHING_PASSWORD'],
    )

def get_creds_from_dict(data: dict) -> Creds:
    return (data['username'], data['password'])

def get_creds_from_obj(obj: object) -> Creds:
    return (obj.username, obj.password)

def get_session(creds: Creds) -> requests.Session:
    session = requests.Session()
    data = {
        'username': creds[0],
        'password': creds[1],
    }
    log.debug(data)
    session.post(
        login_url, data=data,
    )
    return session

get_session_from_env = compose_left(
    get_creds_from_env,
    get_session,
)

get_session_from_dict = compose_left(
    get_creds_from_dict,
    get_session,
)

get_session_from_obj = compose_left(
    get_creds_from_obj,
    get_session,
)

