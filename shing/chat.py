from pathlib import Path
import os
from socket import socket
import urllib.parse
import hashlib
from datetime import datetime
import random
import pprint
import re
import time
from collections import defaultdict
import typing as T

import requests
import socketio
import pyttsx3
import ppt
from ppt.toolz import *

from .common import shing_url

from .channel import channel_url, get_channel_data

log = ppt.logging.new_log(__name__)

class Chat(T.TypedDict):
    name: str
    content: str
    sticker: str
    ts: str
    dt: datetime
    hash: str

class Voice(T.TypedDict):
    name: str
    nationality: str
    sex: str
    id: str

UserName = T.NewType('User', str)

VoiceMap = T.Dict[UserName, Voice]

voices: T.Sequence[Voice] = [
    {
        'name': 'David',
        'nationality': 'United States',
        'sex': 'male',
        'id': 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Speech\\Voices\\Tokens\\MSTTS_V110_enUS_DavidM'
    },
    {
        'name': 'Mark',
        'nationality': 'United States',
        'sex': 'male',
        'id': 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Speech\\Voices\\Tokens\\MSTTS_V110_enUS_MarkM'
    },
    {
        'name': 'Zira',
        'nationality': 'United States',
        'sex': 'female',
        'id': 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Speech\\Voices\\Tokens\\MSTTS_V110_enUS_ZiraM'
    },
    {
        'name': 'James',
        'nationality': 'Australia',
        'sex': 'male',
        'id': 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Speech\\Voices\\Tokens\\MSTTS_V110_enAU_JamesM'
    },
    {
        'name': 'Linda',
        'nationality': 'Canada',
        'sex': 'female',
        'id': 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Speech\\Voices\\Tokens\\MSTTS_V110_enCA_LindaM'
    },
    {
        'name': 'Richard',
        'nationality': 'Canada',
        'sex': 'male',
        'id': 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Speech\\Voices\\Tokens\\MSTTS_V110_enCA_RichardM'
    },
    {
        'name': 'George',
        'nationality': 'United Kingdom',
        'sex': 'male',
        'id': 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Speech\\Voices\\Tokens\\MSTTS_V110_enGB_GeorgeM'
    },
    {
        'name': 'Hazel',
        'nationality': 'United Kingdom',
        'sex': 'female',
        'id': 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Speech\\Voices\\Tokens\\MSTTS_V110_enGB_HazelM'
    },
    {
        'name': 'Susan',
        'nationality': 'United Kingdom',
        'sex': 'female',
        'id': 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Speech\\Voices\\Tokens\\MSTTS_V110_enGB_SusanM'
    },
    {
        'name': 'Sean',
        'nationality': 'Ireland',
        'sex': 'male',
        'id': 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Speech\\Voices\\Tokens\\MSTTS_V110_enIE_SeanM'
    },
    {
        'name': 'Catherine',
        'nationality': 'Australia',
        'sex': 'female',
        'id': 'HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Speech\\Voices\\Tokens\\MSTTS_V110_enAU_CatherineM'
    },
]

default_voice = {
    'male': 'Mark',
    'female': 'Zira'
}

random_voice = lambda: random.choice(voices)

def chat_url(name: str):
    return urllib.parse.urljoin(channel_url(name) + '/', 'chat')

channel_soup = compose_left(channel_url, soup_from_url)

@curry
def chat_attr(name: str, e, default=''):
    return maybe(e.find('div', class_=f'chat-{name}')).or_else(default)

text = lambda t: (t.text or '').strip()
chat_name = compose_left(chat_attr('username'), text)
chat_content = compose_left(chat_attr('content'), text)
chat_ts = compose_left(chat_attr('timestamp'), text)
chat_sticker = compose_left(
    chat_attr('sticker', default={}),
    lambda d: f"{d.get('title', '').strip(':')}",
)

def chats(name: str) -> T.Iterable[Chat]:
    soup = channel_soup(name)
    for m in soup.find_all('div', class_='chat-message'):
        yield pipe(
            {
                'name': chat_name(m),
                'content': chat_content(m),
                'ts': chat_ts(m),
                'sticker': chat_sticker(m),
            },
            lambda d: merge(d, {
                'dt': pipe(
                    d['ts'], 
                    maybe_dt,
                ),
            }), 
            lambda d: merge(d, {
                "hash": dict_hash(hashlib.md5, d)
            }),
        )

callbacks = {
    'start-chat': [],
    'start-word': [],
    'end-chat': [],
}

def start_chat(func):
    callbacks['start-chat'].append(func)
    return func

def start_word(func):
    callbacks['start-word'].append(func)
    return func

def end_chat(func):
    callbacks['end-chat'].append(func)
    return func

def error(func):
    callbacks['error'].append(func)

class ChatEngine:
    engine: pyttsx3.Engine
    callbacks: T.Dict[str, T.List[T.Callable]]
    voices_map = VoiceMap
    users_seen = T.Set

    def __init__(self, voice_map = None):
        self.engine = pyttsx3.init()
        self.callbacks = {
            'start-chat': [],
            'start-word': [],
            'end-chat': [],
            'error': [],
        }
        self.engine.connect('started-utterance', self._start_chat)
        self.engine.connect('started-word', self._start_word)
        self.engine.connect('finished-utterance', self._end_chat)
        self.engine.connect('error', self._error)

        self.voice_map = voice_map or {}
        self.users_seen = set()

    def _start_chat(self, name: str):
        for cb in callbacks['start-chat']:
            cb(name)
        for cb in self.callbacks['start-chat']:
            cb(name)
    def start_chat(self, func):
        self.callbacks['start-chat'].append(func)
        return func

    def _start_word(self, name: str, location: int, length: int):
        for cb in callbacks['start-word']:
            cb(name, location, length)
        for cb in self.callbacks['start-word']:
            cb(name, location, length)
    def start_word(self, func):
        self.callbacks['start-word'].append(func)
        return func

    def _end_chat(self, name: str, completed: bool):
        for cb in callbacks['end-chat']:
            cb(name, completed)
        for cb in self.callbacks['end-chat']:
            cb(name, completed)
    def end_chat(self, func):
        self.callbacks['end-chat'].append(func)
        return func

    def _error(self, name: str, exception: Exception):
        for cb in callbacks['error']:
            cb(name, exception)
        for cb in self.callbacks['error']:
            cb(name, exception)
    def error(self, func):
        self.callbacks['error'].append(func)
        return func

    _last_user = None
    def say(self, chat: Chat):
        user, content, hash = chat['name'], chat['content'], chat['hash']
        if not content.strip():
            content = f"{chat['sticker']} sticker"

        voice = self.voice_map.setdefault(chat['name'], random_voice())
        self.engine.setProperty('voice', voice['id'])

        if not user in self.users_seen:
            self.engine.say(f'New User: {user}', name='new-user')
            self.users_seen.add(user)
        elif user != self._last_user:
            log.info(f'{user} {self._last_user}')
            self.engine.say(f'User: {user}', name='user-change')
            self._last_user = user

        self.engine.say(f"{content}", name=hash)

    def run(self, arg):
        self.engine.runAndWait()
        return arg

def init_engine():
    return ChatEngine()

@start_chat
def log_chat(name: str):
    log.info(f'Starting chat: {name}')

def get_token(session: requests.Session):
    match ppt.rest.get_json(session.get(shing_url('auth/socket-token'))):
        case {'success': True, 'token': token}:
            return token

PollAnswers = T.Sequence[str]

def poll_chat_content(question: str, answers: PollAnswers):
    return pipe(
        [
            f'Poll: {question}',
            pipe(
                answers,
                enumerate,
                vmap(lambda i, a: f'{i + 1}. {a}'),
                '\n'.join,
            )
        ],
        '\n\n'.join
    )

def close_poll_content(slug: str):
    poll = _polls[slug]
    return pipe(
        [
            f'Results of poll: {poll["question"]}',
            pipe(
                poll['answers'],
                enumerate,
                vmap(lambda i, a: f'{i}. {a}: {poll["votes"][i]}'),
                '\n'.join,
            ),
        ],
        '\n\n'.join,
    )

_polls = {}
_current_poll = None
_poll_active = False
def new_poll(client: socketio.Client, slug: str, question: str, 
             answers: T.Sequence[str]):
    global _poll_active, _current_poll
    if not _poll_active:
        poll = _polls[slug] = {
            'slug': slug, 'question': question, 'answers': answers,
            'votes': defaultdict(int),
        }
        content = poll_chat_content(question, answers)
        _current_poll = slug
        _poll_active = True
        log.info(content)
        send_chat(client, content)

def create_poll(client: socketio.Client, args: str):
    log.info(f'Create poll: {args}')
    question, *answers = pipe(
        args,
        csv_rows_from_content(header=False),
        tuple,
        lambda t: t[0] if t and len(t) > 0 else (None,),
    )
    if question and answers:
        log.info(
            f'{client} {question} {answers}'
        )
        new_poll(client, question, question, answers)
    else:
        log.error(
            f'{question} {answers}'
        )

def cast_vote(client: socketio.Client, vote_args: str, ):
    match (_current_poll, vote_args.strip()):
        case (None, _):
            pass
        case (slug, vi) if is_int(vi):
            vi = int(vi) - 1
            poll = _polls[slug]
            if 0 <= vi < len(poll['answers']):
                poll['votes'][int(vi)] += 1
                log.info(
                    '\n' + pprint.pformat(poll)
                )

def close_poll(client: socketio.Client, slug: str):
    global _current_poll
    if _current_poll:
        _current_poll = None
        _poll_active = False
        content = close_poll_content(slug)
        log.info(content)
        send_chat(client, content)

def handle_bot_command(client: socketio.Client, command: dict) -> bool:
    match(command):
        case {'command': 'hello', 'user': {'displayName': user_name}}:
            send_chat(client, f'Hello {user_name}! How are you?')
        case {'command': 'vote', 'args': args, 
              'user': {'displayName': user_name}}:
            cast_vote(client, args)
        case {'command': 'createpoll', 'args': args, 
              'user': {'displayName': user_name},
              'channel': {
                'owner': {
                    'displayName': owner_name
                }
               }} if owner_name == user_name:
            log.info(f'Bleh? {owner_name}, {user_name}')
            create_poll(client, args)
            #send_chat(client, f'Hello {name}! How are you?')

bot_re = re.compile(
    r'^/(?P<command>\S+)\s+(?P<args>.*)$'
)

def get_chat_client(session: requests.Session, channel: str):
    client = socketio.Client(
        # logger=True, engineio_logger=True
    )

    channel_data = get_channel_data(session, channel)
    token = get_token(session)

    @client.event
    def connect():
        log.info(f'Socket IO for {channel} connected')

    @client.on('user-chat')
    def user_chat(data):
        match data:
            case {
                # '_id': chat_id,
                'content': content,
                'stickers': stickers,
                'user': {
                    # '_id': user_id,
                    'displayName': display_name,
                    # 'username': username,
                } as user}:
                log.info(
                    f'Chat from {display_name}:'
                )
                if content:
                    log.info(
                        f'Content: {repr(content)}'
                    )
                if stickers:
                    log.info('Stickers:')
                    pipe(
                        stickers,
                        map(lambda s: (
                            f'   type: {s["ownerType"]}'
                            f' slug: {s["slug"]}'
                        )),
                        map(log.info),
                        tuple,
                    )

                bot_command = groupdict(bot_re, soup(content).text)
                if bot_command:
                    command = merge(
                        bot_command,
                        {'user': user},
                        {'channel': channel_data},
                    )
                    log.info(
                        f'Bot command: {pprint.pformat(bot_command)}'
                    )
                    handle_bot_command(client, command)


    @client.event
    def disconnect():
        log.info(f'Socket IO for {channel} disconnected')

    client.connect(
        'wss://shing.tv/', transports='websocket', auth={'token': token}
    )

    client.emit('join', {'channelId': channel_data['_id']})
    client.channelId = channel_data['_id']

    return client

def send_chat(client: socketio.Client, chat: str):
    time.sleep(1)
    client.emit('user-chat', {'channelId': client.channelId, 'content': chat})
