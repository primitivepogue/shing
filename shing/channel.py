from pathlib import Path
import urllib.parse
import requests
import os
import re
import typing as T

import bs4
import ppt
from ppt.toolz import *

from .common import (
    shing_url
)

def channel_url(name: str):
    '''
    Examples:

    >>> channel_url('test')
    'https://shing.tv/channel/test'
    '''
    return shing_url('channel/', f'{name}')

@curry
def channel_soup(session: requests.Session, name: str):
    return pipe(
        session.get(channel_url(name)),
        deref('text'),
        lambda d: bs4.BeautifulSoup(d, 'lxml'),
    )

@curry
def get_channel_data(session: requests.Session, name: str):
    soup = channel_soup(session, name)
    data_re = re.compile(r'dtp.channel = (?P<data>{.*?});')
    return pipe(
        soup.find_all('script'),
        map(deref('text')),
        filter(data_re.search),
        first,
        groupdict(data_re),
        get('data'),
        json_loads
    )