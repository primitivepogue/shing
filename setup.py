import re
from pathlib import Path

from setuptools import setup, find_packages

HERE = Path(__file__).resolve().parent

version_re = re.compile(r"^__version__\s*=\s*'(?P<version>.*)'$", re.M)

def version():
    match = version_re.search(Path('shing/__init__.py').read_text())
    if match:
        return match.groupdict()['version'].strip()
    raise AttributeError(
        'Could not find __version__ attribute in shing/__init__.py'
    )

long_description = Path(HERE, 'README.md').resolve().read_text()

setup(
    name='shing',
    packages=find_packages(
        exclude=['tests'],
    ),
    package_dir={
        'shing': 'shing',
    },
    package_data = {
        'shing': [
            'data/*.txt',
            'data/*.csv',
            'data/*.gz',
        ],
    },

    install_requires=[
        'pptoolz @ git+https://gitlab.com/primitivepogue/pptoolz',
        'bs4',
        'click',
        'lxml',
        'requests',
        'requests[socks]',
    ],

    version=version(),
    description=(
        'Shing.tv tools'
    ),
    long_description=long_description,
    long_description_content_type='text/markdown',

    url='https://gitlab.com/primitivepogue/shing.git',

    author="Pogue Mahone",
    author_email='primitivepogue@protonmail.com',

    license='MIT',

    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.10',
    ],

    zip_safe=False,

    keywords=('many things'),

    scripts=[
    ],

    entry_points={
        'console_scripts': [
        ],
    },
)
